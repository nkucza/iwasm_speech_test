include(FindPackageHandleStandardArgs)
unset(TFLITE_FOUND)

find_path(TFLITEWASM_INCLUDE_DIR
  NAMES
    tensorflow/lite
    third_party
  HINTS
    ${TFLITEWASM_ROOT}
    ${TFLITEWASM_ROOT}/include
    ${TFLITEWASM_ROOT}/tensorflow/lite
)

find_path(TFLITEWASM_LIBRARY_DIR
  NAMES
    "libtensorflow-lite.a"
  HINTS
    ${TFLITEWASM_ROOT}
    ${TFLITEWASM_ROOT}/lib
    ${TFLITEWASM_ROOT}/tensorflow/lite
)

find_library(TFLITEWASM_LIBRARY
  NAMES
    "libtensorflow-lite.a"
  HINTS
    ${TFLITEWASM_ROOT}
    ${TFLITEWASM_ROOT}/lib
    ${TFLITEWASM_ROOT}/tensorflow/lite
)

# find_path(TFLITEWASM_SCHEMA_INCLUDE_DIR
#     schema_generated.h
#   HINTS
#     ${TFLITEWASM_ROOT}/tensorflow/lite/schema
# )

set(TFLITEWASM_LIBRARY_NAME "tensorflow-lite")

## Set TFLITE_FOUND
# find_package_handle_standard_args(TfliteWasm DEFAULT_MSG TFLITEWASM_INCLUDE_DIR TFLITEWASM_LIBRARY TFLITEWASM_SCHEMA_INCLUDE_DIR)
find_package_handle_standard_args(TfliteWasm DEFAULT_MSG TFLITEWASM_INCLUDE_DIR TFLITEWASM_LIBRARY TFLITEWASM_LIBRARY_NAME)

## Set external variables for usage in CMakeLists.txt
if(TFLITE_FOUND)
  set(TFLITEWASM_LIBRARY ${TFLITEWASM_LIBRARY})
  set(TFLITEWASM_INCLUDE_DIR ${TFLITEWASM_INCLUDE_DIR})
  # set(TFLITEWASM_SCHEMA_INCLUDE_DIR ${TFLITEWASM_SCHEMA_INCLUDE_DIR})
  set(TFLITEWASM_LIBRARY_NAME ${TFLITEWASM_LIBRARY_NAME})
endif()