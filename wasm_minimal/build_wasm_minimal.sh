#!/bin/bash

echo -e "Build TensorFlow Lite wasm minimal example"

BUILD_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
BUILD_DIR="${BUILD_SCRIPT_DIR}/wasm_minimal_build"
SRC_DIR="${BUILD_SCRIPT_DIR}/wasm_minimal_src"
EMSCRIPTEN_ROOT="${BUILD_SCRIPT_DIR}/wasm-micro-runtime/core/deps/emsdk/upstream/emscripten"
EMSCRIPTEN_TOOLCHAIN_FILE="${BUILD_SCRIPT_DIR}/wasm-micro-runtime/core/deps/emsdk/upstream/emscripten/cmake/Modules/Platform/Emscripten.cmake"
EMSCRIPTEN_WASM_DIR="${EMSCRIPTEN_ROOT}/cache/sysroot/lib/wasm32-emscripten"
CUSTOM_LIBC_DIR="${EMSCRIPTEN_WASM_DIR}"

if [[ ! -d "${BUILD_DIR}" ]]; then
  mkdir -p ${BUILD_DIR}
fi

TENSORFLOW_SOURCE_DIR="${BUILD_SCRIPT_DIR}/tflite_wasm"
EM_COMPILER_DIR="${EMSCRIPTEN_ROOT}"
EM_AR=$EM_COMPILER_DIR/emar
EM_RANLIB=$EM_COMPILER_DIR/emranlib

# Restore the libc.a in EMSDK_WASM_DIR
function restore_em_libc() {
  if [[ -e "${EMSCRIPTEN_WASM_DIR}/libc.a.bak" ]]; then
    cd ${EMSCRIPTEN_WASM_DIR}
    mv libc.a.bak libc.a
    echo "Emscripten libc.a restored"
  fi
}

# Set up SIGINT trap to call function on user interrupt
function exitfn() {
  echo "Script interrupted"
  # Restore modified libc.a
  restore_em_libc
  # Restore signal handling for SIGINT
  trap SIGINT
  # Exit script
  exit 1
}
trap "exitfn" INT

# Hack emcc
if [[ ! -e "${EMSCRIPTEN_WASM_DIR}/libc.a.bak" ]]; then
  cd ${EMSCRIPTEN_WASM_DIR}
  # back up libc.a
  cp libc.a libc.a.bak
  # delete some objects in libc.a
  $EM_AR d libc.a open.o
  $EM_AR d libc.a mmap.o
  $EM_AR d libc.a munmap.o
  $EM_AR d libc.a library_pthread_stub.o
  $EM_AR d libc.a pthread_self.o
  $EM_AR d libc.a fopen.o
  $EM_AR d libc.a fread.o
  $EM_AR d libc.a fseeko.o
  $EM_AR d libc.a emcc_fwrite.o
  $EM_AR d libc.a feof.o
  $EM_RANLIB libc.a
fi

# Build wasm minimal with emscripten compiler
cd ${BUILD_DIR}
# cmake ${SRC_DIR} -DTENSORFLOW_SOURCE_DIR=${TENSORFLOW_SOURCE_DIR} -G "Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE=${EMSCRIPTEN_TOOLCHAIN_FILE} -DEMSCRIPTEN_ROOT_PATH=${EMSCRIPTEN_ROOT} --debug-find
cmake ${SRC_DIR} -DTENSORFLOW_SOURCE_DIR=${TENSORFLOW_SOURCE_DIR} -DEM_COMPILER_DIR=${EM_COMPILER_DIR} -DCUSTOM_LIBC_DIR=${CUSTOM_LIBC_DIR}
cmake --build . -j $(nproc)

if [[ ! -e "${BUILD_DIR}/wasm_minimal.wasm" ]]; then
  echo "Build failed"
  exit 1
fi

# Compile .wasm to .aot
echo -e "\nCompile TensorFlow Lite minimal example .wasm to .aot"
#WAMRC_CMD="${BUILD_SCRIPT_DIR}/wasm-micro-runtime/wamr-compiler/wamrc"
WAMRC_CMD="${BUILD_SCRIPT_DIR}/wamrc/wamrc"

cd ${BUILD_DIR}
if [[ $1 == '--sgx' ]]; then
  ${WAMRC_CMD} -sgx -o wasm_minimal.aot wasm_minimal.wasm
elif [[  $1 == '--threads' ]]; then
  ${WAMRC_CMD} --enable-multi-thread -o wasm_minimal.aot wasm_minimal.wasm
else
  ${WAMRC_CMD} -o wasm_minimal.aot wasm_minimal.wasm
fi

# Restore Emscripten libc.a
restore_em_libc

cp $BUILD_SCRIPT_DIR/../models/filters_vocab_multilingual.bin $BUILD_DIR/filters_vocab_gen.bin
cp $BUILD_SCRIPT_DIR/../samples/jfk.wav $BUILD_DIR/jfk.wav
cp $BUILD_SCRIPT_DIR/../models/whisper.tflite $BUILD_DIR
cp $BUILD_SCRIPT_DIR/../models/whisper-medium.tflite $BUILD_DIR
cp $BUILD_SCRIPT_DIR/../models/whisper-small.tflite $BUILD_DIR
#cp $BUILD_SCRIPT_DIR/run_wasm_minimal.sh $BUILD_DIR

echo "Build finished"
