#!/bin/bash

echo -e "Build TensorFlow Lite example"

BUILD_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

#WAMR_DIR="/home/ubuntu/wasm-micro-runtime"
WAMR_DIR="${BUILD_SCRIPT_DIR}/wasm-micro-runtime"
WAMR_PLATFORM_DIR="${WAMR_DIR}/product-mini/platforms"
WAMRC_DIR="${WAMR_DIR}/wamr-compiler"
WAMRC_BUILD_DIR="${BUILD_SCRIPT_DIR}/wamrc"

CORE_DEPS_DIR="${WAMR_DIR}/core/deps"
EMSDK_DIR="${CORE_DEPS_DIR}/emsdk"
WAMR_BUILD_DIR="${BUILD_SCRIPT_DIR}/wamr_build"

EMSDK_WASM_DIR="${EMSDK_DIR}/upstream/emscripten/cache/sysroot/lib/wasm32-emscripten"
OUT_DIR="${BUILD_SCRIPT_DIR}/out"
TENSORFLOW_DIR="${BUILD_SCRIPT_DIR}/tensorflow"
TFLITE_BUILD_DIR="${TENSORFLOW_DIR}/tensorflow/lite/tools/make"

TFLITE_INSTALL_DIR="${BUILD_SCRIPT_DIR}/tflite_wasm"
WAMR_INSTALL_DIR="${BUILD_SCRIPT_DIR}/wamr_lib"

TFLITE_INCLUDE_DIR="${TFLITE_INSTALL_DIR}/include"
TFLITE_LIB_DIR="${TFLITE_INSTALL_DIR}/lib"

IWASM_BUILD_DIR="${BUILD_SCRIPT_DIR}/iwasm"

function Clear_Before_Exit()
{
    [[ -f ${TENSORFLOW_DIR}/tf_lite.patch ]] &&
       rm -f ${TENSORFLOW_DIR}/tf_lite.patch
    # resume the libc.a under EMSDK_WASM_DIR
    cd ${EMSDK_WASM_DIR}
    mv libc.a.bak libc.a
}

#set -xe
set -e

# Install WASM runtime 
echo -e "\nInstall WASM runtime"
if [[ ! -d "${WAMR_DIR}" ]]; then
  git clone --depth 1 https://github.com/bytecodealliance/wasm-micro-runtime
fi
cd ${WAMR_DIR}/product-mini/platforms/linux/
# if [[ -d "build" ]]; then
# 	rm -rf build
# fi
# mkdir build && cd build
if [[ ! -d "${WAMR_BUILD_DIR}" ]]; then
  mkdir -p ${WAMR_BUILD_DIR}
fi
# cmake ..
cmake ${WAMR_DIR}/product-mini/platforms/linux/
make -j $(nproc)
make DESTDIR=${WAMR_INSTALL_DIR} install

# 1.clone emsdk
echo -e "\nInstall emscriptem SDK"
cd ${CORE_DEPS_DIR}
# if [ -d "${BUILD_SCRIPT_DIR}" ]; then
#   rm -fr emsdk
# fi
if [ ! -d "${CORE_DEPS_DIR}/emsdk" ]; then
  git clone https://github.com/emscripten-core/emsdk.git
fi
cd ${CORE_DEPS_DIR}/emsdk
./emsdk install 2.0.26
./emsdk activate 2.0.26
source emsdk_env.sh

# 2.hack emcc
cd ${EMSDK_WASM_DIR}
# back up libc.a
cp libc.a libc.a.bak
# delete some objects in libc.a
emar d libc.a open.o
emar d libc.a mmap.o
emar d libc.a munmap.o
emar d libc.a library_pthread_stub.o
emar d libc.a pthread_self.o
emar d libc.a fopen.o
emar d libc.a fread.o
emar d libc.a fseeko.o
emar d libc.a emcc_fwrite.o
emar d libc.a feof.o
emranlib libc.a

# 3. build tf-lite
echo -e "\nBuild TensorFlow Lite"
cd ${BUILD_SCRIPT_DIR}
# 3.1 clone tf repo from Github and checkout to 2303ed commit
if [ ! -d "tensorflow" ]; then
    git clone https://github.com/tensorflow/tensorflow.git
fi

cd ${TENSORFLOW_DIR}
git checkout 2303ed4bdb344a1fc4545658d1df6d9ce20331dd

# 3.2 copy the tf-lite.patch to tensorflow_root_dir and apply it
cd ${TENSORFLOW_DIR}
cp ${BUILD_SCRIPT_DIR}/tf_lite.patch .
git checkout tensorflow/lite/tools/make/Makefile
git checkout tensorflow/lite/tools/make/targets/linux_makefile.inc

cp $BUILD_SCRIPT_DIR/tflite_minimal/minimal.cc  $TENSORFLOW_DIR/tensorflow/lite/examples/minimal/
cp $BUILD_SCRIPT_DIR/tflite_minimal/*.h  $TENSORFLOW_DIR/tensorflow/lite/examples/minimal/


if [[ $(git apply tf_lite.patch 2>&1) =~ "error" ]]; then
    echo "git apply patch failed, please check tf-lite related changes..."
    Clear_Before_Exit
    exit 0
fi

# Copy modified libc.a to library dir
find ${EMSDK_WASM_DIR} -name "libc.a" -exec bash -c "cp -p {} ${TFLITE_LIB_DIR} " \;

cd ${TFLITE_BUILD_DIR}
# 3.3 download dependencies
if [ ! -d "${TFLITE_BUILD_DIR}/downloads" ]; then
    source download_dependencies.sh
fi

# 3.4 build tf-lite target
if [ -d "${TFLITE_BUILD_DIR}/gen" ]; then
    rm -fr ${TFLITE_BUILD_DIR}/gen
fi

# make -j 4 -C "${TENSORFLOW_DIR}" -f ${TFLITE_BUILD_DIR}/Makefile
make -j $(nproc) -C "${TENSORFLOW_DIR}" -f ${TFLITE_BUILD_DIR}/Makefile
# make -j $(nproc) -C "${TENSORFLOW_DIR}" -f ${TFLITE_BUILD_DIR}/Makefile DESTDIR=${TFLITE_INSTALL_DIR} install

# remove patch file and recover emcc libc.a after building
Clear_Before_Exit

# 3.5 copy /make/gen target files to out/
rm -rf ${OUT_DIR}
mkdir ${OUT_DIR}
cp -r ${TFLITE_BUILD_DIR}/gen/linux_x86_64/bin/. ${OUT_DIR}/

# Copy tflie header files
if [[ ! -d "${TFLITE_INCLUDE_DIR}" ]]; then
  mkdir -p ${TFLITE_INCLUDE_DIR}
fi
if [[ ! -d "${TFLITE_LIB_DIR}" ]]; then
  mkdir -p ${TFLITE_LIB_DIR}
fi
cd ${TENSORFLOW_DIR}
find tensorflow/lite -name "*.h" -exec bash -c "cp -p --parents {} ${TFLITE_INCLUDE_DIR} " \;
find tensorflow/lite -name "libtensorflow-lite.a" -exec bash -c "cp -p {} ${TFLITE_LIB_DIR} " \;



exit 0



# 4. Build wamr compiler
echo -e "\nBuild WAMR compiler"
cd ${WAMRC_DIR}
#./build_llvm.sh
./build_llvm.sh --extra-cmake-flags="-DLLVM_CCACHE_BUILD:BOOL=OFF"
if [[ ! -d "${WAMRC_BUILD_DIR}" ]]; then\
  mkdir -p ${WAMRC_BUILD_DIR}
else
  rm -rf ${WAMRC_BUILD_DIR}
fi
# rm -fr build && mkdir build
# cd build && cmake ..
# make
cd ${WAMRC_BUILD_DIR}
cmake ${WAMRC_DIR}
cmake --build . -j $(nproc)
WAMRC_CMD=${WAMRC_BUILD_DIR}/wamrc


# 5. build iwasm with pthread and libc_emcc enable
echo -e "\nBuild IWASM runtime"
#    platform:
#     linux by default
#     linux-sgx if $1 equals '--sgx'
# if [[ $1 == '--sgx' ]]; then
#   cd ${WAMR_PLATFORM_DIR}/linux-sgx
#   rm -fr build && mkdir build
#   cd build && cmake .. -DWAMR_BUILD_LIBC_EMCC=1
#   make
#   cd ../enclave-sample
#   make
# else
#   cd ${WAMR_PLATFORM_DIR}/linux
#   rm -fr build && mkdir build
#   cd build && cmake .. -DWAMR_BUILD_LIB_PTHREAD=1 -DWAMR_BUILD_LIBC_EMCC=1
#   make
# fi
if [[ -d "${IWASM_BUILD_DIR}" ]]; then
  rm -rf ${IWASM_BUILD_DIR}
fi
mkdir -p ${IWASM_BUILD_DIR}
cd ${IWASM_BUILD_DIR}
if [[ $1 == '--sgx' ]]; then
  cmake ${WAMR_PLATFORM_DIR}/linux-sgx -DWAMR_BUILD_LIBC_EMCC=1
  cmake --build . -j $(nproc)
  cd ${WAMR_PLATFORM_DIR}/linux-sgx
  cmake --build . -j $(nproc)
else
  cmake ${WAMR_PLATFORM_DIR}/linux -DWAMR_BUILD_LIB_PTHREAD=1 -DWAMR_BUILD_LIBC_EMCC=1
  cmake --build . -j $(nproc)
fi
