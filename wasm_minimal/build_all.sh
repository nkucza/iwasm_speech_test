#!/bin/bash

mkdir -p log
echo -e "Step 1/4: Build TFLite WASM"
./build_tflite_wasm.sh &> log/build_tflite_wasm.log
echo -e "Step 2/4: Build WAMRC"
./build_wamrc.sh &> log/build_wamrc.log
echo -e "Step 3/4: Build IWASM"
./build_iwasm.sh &> log/build_iwasm.log
echo -e "Step 4/4: Build application"
./build_wasm_minimal.sh &> log/build_wasm_minimal.log
