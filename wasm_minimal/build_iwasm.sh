#!/bin/bash

echo -e "Build WAMR compiler"

BUILD_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
WAMR_DIR="${BUILD_SCRIPT_DIR}/wasm-micro-runtime"
WAMRC_DIR="${WAMR_DIR}/wamr-compiler"
WAMR_PLATFORM_DIR="${WAMR_DIR}/product-mini/platforms"
IWASM_BUILD_DIR="${BUILD_SCRIPT_DIR}/iwasm"



# 5. build iwasm with pthread and libc_emcc enable
echo -e "\nBuild IWASM runtime"
#    platform:
#     linux by default
#     linux-sgx if $1 equals '--sgx'

# if [[ $1 == '--sgx' ]]; then
#   cd ${WAMR_PLATFORM_DIR}/linux-sgx
#   rm -fr build && mkdir build
#   cd build && cmake .. -DWAMR_BUILD_LIBC_EMCC=1
#   make
#   cd ../enclave-sample
#   make
# else
#   cd ${WAMR_PLATFORM_DIR}/linux
#   rm -fr build && mkdir build
#   cd build && cmake .. -DWAMR_BUILD_LIB_PTHREAD=1 -DWAMR_BUILD_LIBC_EMCC=1
#   make
# fi

if [[ -d "${IWASM_BUILD_DIR}" ]]; then
  rm -rf ${IWASM_BUILD_DIR}
fi
mkdir -p ${IWASM_BUILD_DIR}
cd ${IWASM_BUILD_DIR}
if [[ $1 == '--sgx' ]]; then
  cmake ${WAMR_PLATFORM_DIR}/linux-sgx -DWAMR_BUILD_LIBC_EMCC=1
  cmake --build . -j $(nproc)
  cd ${WAMR_PLATFORM_DIR}/linux-sgx
  cmake --build . -j $(nproc)
else
  cmake ${WAMR_PLATFORM_DIR}/linux -DWAMR_BUILD_LIB_PTHREAD=1 -DWAMR_BUILD_LIBC_EMCC=1
  cmake --build . -j $(nproc)
fi
