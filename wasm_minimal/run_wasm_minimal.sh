#!/bin/bash

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
BUILD_DIR="$SCRIPT_DIR/wasm_minimal_build"

# WAMR_DIR="${SCRIPT_DIR}/../wasm-micro-runtime"
# WAMR_PLATFORM_DIR="${WAMR_DIR}/product-mini/platforms"

# ./wamr_lib/usr/local/bin/iwasm
# ./wasm-micro-runtime/core/iwasm

# if [[ $1 == '--sgx' ]]; then
#   IWASM_CMD="${WAMR_PLATFORM_DIR}/linux-sgx/enclave-sample/iwasm"
# else
#   IWASM_CMD="${WAMR_PLATFORM_DIR}/linux/build/iwasm"
# fi

# if [[ $1 == '--threads' ]]; then
#   ${IWASM_CMD} --heap-size=10475860 \
#             wasm_minimal.aot --num_threads=4 \
#             --graph=whisper-small.tflite --max_secs=300
# else
#   ${IWASM_CMD} --heap-size=10475860 \
#             wasm_minimal.aot \
#             --graph=whisper-small.tflite --max_secs=300
# fi

IWASM_CMD="${SCRIPT_DIR}/wamr_lib/usr/local/bin/iwasm"

# ./wasm-micro-runtime/product-mini/platforms/linux/build/iwasm wasm_minimal.aot whisper-small.tflite jfk.wav

cd $BUILD_DIR
AOT_FILE=$(realpath $BUILD_DIR/wasm_minimal.aot)
MODEL_FILE=$(realpath $BUILD_DIR/whisper-small.tflite)
SOUND_FILE=$(realpath $BUILD_DIR/jfk.wav)
echo "AOT_FILE = $AOT_FILE"
echo "MODEL_FILE = $MODEL_FILE"
echo "SOUND_FILE = $SOUND_FILE"
#${IWASM_CMD} wasm_minimal.aot whisper-small.tflite jfk.wav
#$IWASM_CMD --heap-size=10475860 $AOT_FILE $MODEL_FILE $SOUND_FILE
$IWASM_CMD $AOT_FILE $MODEL_FILE $SOUND_FILE
