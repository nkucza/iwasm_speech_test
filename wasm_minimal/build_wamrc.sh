#!/bin/bash

echo -e "Build WAMR compiler"

BUILD_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
WAMR_DIR="${BUILD_SCRIPT_DIR}/wasm-micro-runtime"
WAMRC_DIR="${WAMR_DIR}/wamr-compiler"
WAMRC_BUILD_DIR="${BUILD_SCRIPT_DIR}/wamrc"


cd ${WAMRC_DIR}
#./build_llvm.sh
./build_llvm.sh --extra-cmake-flags="-DLLVM_CCACHE_BUILD:BOOL=OFF"
if [[ ! -d "${WAMRC_BUILD_DIR}" ]]; then\
  mkdir -p ${WAMRC_BUILD_DIR}
else
  rm -rf ${WAMRC_BUILD_DIR}
fi
# rm -fr build && mkdir build
# cd build && cmake ..
# make
cd ${WAMRC_BUILD_DIR}
cmake ${WAMRC_DIR}
cmake --build . -j $(nproc)
WAMRC_CMD=${WAMRC_BUILD_DIR}/wamrc

